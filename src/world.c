#include "world.h"

void world_resolve_collisions(world *w)
{
	int  x, y, z, j;
	ivector pos_box_projectile;
	ivector pos_box_target;
	unsigned int index_box_projectile, index_box_target;

	const ivector neighbours[13] = {
		{1,0,0}, {1,0,-1}, {0,0,-1}, {-1,0,-1},
		{1,1,1}, {0,1,1}, {-1,1,1},
		{1,1,0}, {0,1,0}, {-1,1,0},
		{1,1,-1}, {0,1,-1}, {-1,1,-1}};

	ball *projectile, *target;

	for (x = 0; x < w->num_boxes.x; x++)
	for (y = 0; y < w->num_boxes.y; y++)
	for (z = 0; z < w->num_boxes.z; z++)	
	{
		pos_box_projectile = (ivector) {x, y, z};
		index_box_projectile = world_box_index(w, pos_box_projectile);
		// Collisions within this box
		projectile = w->boxes[index_box_projectile];
		while (NULL != projectile)
		{
			target = projectile->next;
			while (NULL != target)
			{
				DEBUG("Collision of ball %d and ball %d?",
					projectile->number, target->number);
					w->num_collisions += ball_collision(projectile, target, w->ball_radius);
				target = target->next;
			}
			projectile = projectile->next;
		}

		// Collisions with surrounding boxes	
		for (j = 0; j < 13; j++)
		{
			ivector_add(&pos_box_projectile, &neighbours[j], &pos_box_target);
			if (world_box_is_valid(w, &pos_box_target))

			{
			
				index_box_target = world_box_index(w, pos_box_target);
				projectile = w->boxes[index_box_projectile];
				while (NULL != projectile)
				{
					target = w->boxes[index_box_target];
					while (NULL != target)
					{
						DEBUG("Collision of ball %d and ball %d?",
							projectile->number, target->number);
						w->num_collisions += ball_collision(projectile, target, w->ball_radius);
						target = target->next;
					}
					projectile = projectile->next;
				}
			}
		}
	}
}

void world_assign_boxes(world *w)
{
	unsigned int i, index;
	ivector pos;
	for (i = 0; i < w->num_boxes_total; i++)
		w->boxes[i] = NULL;

	for (i = 0; i < w->num_balls; i++)
	{
		pos.x = w->balls[i].r.x / w->box_width;
		pos.y = w->balls[i].r.y / w->box_width;
		pos.z = w->balls[i].r.z / w->box_width;

		index = world_box_index(w, pos);
		if (NULL == w->boxes[index])
		{
			w->balls[i].next = NULL;
			w->boxes[index] = &w->balls[i];
		}
		else
		{
			w->balls[i].next = w->boxes[index];
			w->boxes[index] = &w->balls[i];
		}
		DEBUG("Ball %d is in box %d.", i, index);
	}
}

void world_wall_collisions(world *w)
{
	const double time = w->timestep * w->num_steps / 2.0;
	int i;
	for(i = 0; i < w->num_balls; i++)
	{
		if(w->balls[i].r.x < 0)
		{
			ball_reflect(&(w->balls[i]), 1);
			w->balls[i].r.x = 0;
			w->mean_force += fabs(w->balls[i].v.x)/time;
		} else if(w->balls[i].r.x > w->boundaries.x)
		{
			ball_reflect(&(w->balls[i]), 1);
			w->balls[i].r.x = w->boundaries.x;
			w->mean_force += fabs(w->balls[i].v.x)/time;
		}

		if(w->balls[i].r.y < 0)
		{
			ball_reflect(&(w->balls[i]), 2);
			w->balls[i].r.y = 0;
			w->mean_force += fabs(w->balls[i].v.y)/time;
		} else if(w->balls[i].r.y > w->boundaries.y)
		{
			ball_reflect(&(w->balls[i]), 2);
			w->balls[i].r.y = w->boundaries.y;
			w->mean_force += fabs(w->balls[i].v.y)/time;
		}

		if(w->balls[i].r.z < 0)
		{
			ball_reflect(&(w->balls[i]), 3);
			w->balls[i].r.z = 0;
			w->mean_force += fabs(w->balls[i].v.z)/time;
			w->mean_force_bottom = fabs(w->balls[i].v.z)/time;
		} else if(w->balls[i].r.z > w->boundaries.z)
		{
			ball_reflect(&(w->balls[i]), 3);
			w->balls[i].r.z = w->boundaries.z;
			w->mean_force += fabs(w->balls[i].v.z)/time;
			w->mean_force_top = fabs(w->balls[i].v.z)/time;
		}
	}	

}


void world_move(world *w)
{
	unsigned int i;

	for (i = 0; i < w->num_balls; i++)
	{
		ball_move(&w->balls[i], w->timestep);
		//ERROR("ball_move() not yet implementet!");
		
		//Gravity
		w->balls[i].v.z -= w->gravity * w->timestep;
	}
	w->step_count++;
}

unsigned int world_box_index(world *w, ivector position)
{
	unsigned int index;
	index = position.x;
	index += w->num_boxes.x * position.y;
	index += w->num_boxes.x * w->num_boxes.y * position.z;
	return index;
}

BOOL world_box_is_valid(world *w, ivector *position)
{
	BOOL xcond, ycond, zcond;
	xcond = (position->x  >= 0 ) && (position->x < w->num_boxes.x);
	ycond = (position->y  >= 0 ) && (position->y < w->num_boxes.y);
	zcond = (position->z  >= 0 ) && (position->z < w->num_boxes.z);

	return xcond && ycond && zcond;
}

void world_free(world *w)
{
	// Free balls
	if (NULL != w->balls)
	{
		free(w->balls);
		w->balls = NULL;
		w->num_balls = 0;
	}
	// Free boxes
	if (NULL != w->boxes)
	{
		free(w->boxes);
		w->boxes = NULL;
		w->num_boxes_total = 0;
		w->num_boxes = (ivector){0,0,0};
	}
}

void world_init(world *w, double timestep, double box_width, double ball_radius)
{	
	w->timestep = timestep;
	w->box_width = box_width;
	w->ball_radius = ball_radius;
	w->gravity = 0;
	w->step_count = 0;
	w->balls = NULL;
	w->num_balls = 0;
	w->boxes = NULL;
	w->num_boxes_total = 0;
	w->num_boxes = (ivector){0,0,0};
	w->boundaries = (vector){0,0,0};
	w->num_collisions = 0;
	w->mean_force = 0;
	w->mean_force_top = 0;
	w->mean_force_bottom = 0;
}

void world_test()
{
	world w;
	world_init(&w, 1, 4, 1);

	world_load_config(&w, "../test.config");
	world_load_from_file(&w, "../test.world");
	DEBUG("Wall Collisions...");
	world_wall_collisions(&w);
	DEBUG("Assign boxes...");
	world_assign_boxes(&w);
	DEBUG("Resolving collisions...");
	world_resolve_collisions(&w);
	world_save_to_povray(&w, "..");
	world_save_to_file(&w, "..");
}

