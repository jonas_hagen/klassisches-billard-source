#include "vector.h"

double vector_scalar_product(vector *a, vector *b)
{
	return a->x * b->x + a->y * b->y + a->z * b->z;
}

void vector_add(vector *a, vector *b, vector *result)
{
	result->x = a->x + b->x;
	result->y = a->y + b->y;
	result->z = a->z + b->z;
}

void vector_subtract(vector *a, vector *b, vector *result)
{
	result->x = a->x - b->x;
	result->y = a->y - b->y;
	result->z = a->z - b->z;
}

double vector_abs_squared(vector *a)
{
	return a->x * a->x + a->y * a->y + a->z * a->z;
}

double vector_abs(vector *a)
{
	double res;
	res = vector_abs_squared(a);
	return sqrt(res);
}

void vector_multiply(double scalar, vector *a, vector *result)
{
	result->x = scalar * a->x;
	result->y = scalar * a->y;
	result->z = scalar * a->z;
}

void vector_normalize(vector *a, vector *result)
{
	double inverseabs;
	inverseabs = 1/vector_abs(a);
	vector_multiply(inverseabs, a, result);
}

void vector_init(vector *a)
{
	a->x = 0;
	a->y = 0;
	a->z = 0;
}

BOOL vector_compare(vector *a, vector *b)
{
	vector diff;
	vector_subtract(a,b,&diff);
	if (diff.x==0 && diff.y==0 && diff.z==0)
       	{
		return TRUE;		
	}
	return FALSE;
}

void ivector_add(ivector *a,const ivector *b, ivector *result)
{
	result->x = a->x + b->x;
	result->y = a->y + b->y;
	result->z = a->z + b->z;
}

void vector_test()
{
	vector a,b,c;
	double res;
	a.x=1;
	a.y=2;
	a.z=0;
	b.x=0;
	b.y=1;
	b.z=2;
	res = vector_scalar_product( &a, &b);
	printf("\n %f \n",res);
	vector_add(&a,&b,&c);
	printf("\n(1,2,0) + (0,1,2) = (%f,%f,%f)\n",c.x,c.y,c.z);
	res = vector_abs_squared(&a);
	printf("\n |(1,2,0)|² = %f \n", res);
	res = vector_abs(&a);
	printf("\n |(1,2,0)| = %f \n", res);
	vector_multiply(3,&a,&c);
	printf("\n 3*(1,2,0) = (%f,%f,%f) \n",c.x,c.y,c.z);
	vector_normalize(&a,&c);
	printf("\n (1,2,0)^ = (%f,%f,%f) \n",c.x,c.y,c.z);
	vector_init(&c);
	printf("\n (%f,%f,%f) \n",c.x,c.y,c.z);
}

void vector_printvector(vector *a)
{
	printf("\n(%f,%f,%f)\n",a->x,a->y,a->z);
	
}
