#ifndef VECTOR_H
#define VECTOR_H
#include <math.h>
#include "global.h"

struct vector_s
{
	double x;
	double y;
	double z;
};

struct ivector_s
{
	int x;
	int y;
	int z;
};

typedef struct ivector_s ivector;
typedef struct vector_s vector;

double vector_scalar_product(vector *a, vector *b);
void vector_add(vector *a, vector *b, vector *result);
void vector_subtract(vector *a, vector *b, vector *result);
double vector_abs_squared(vector *a);
double vector_abs(vector *a);
void vector_multiply(double scalar, vector *a, vector *result);
void vector_normalize(vector *a, vector *result);
void vector_init(vector *a);
/*
 *Returns TRUE if the diffrence of vectors a and b is the zero vector.
 */
BOOL vector_compare(vector *a, vector *b);
void ivector_add(ivector *a,const ivector *b, ivector *result);

void vector_printvector(vector *a);
void vector_test();

#endif /* VECTOR_H */
