#include "statistics.h"
#include "statistics.h"


double statistics_total_energy(int num_balls, ball *balls)
{
	int i;
	double total = 0;
	for (i = 0; i < num_balls; i++)
	{
		total += vector_abs_squared(&balls[i].v) / 2.0;
	}
	return total;
}

double statistics_total_momentum(int num_balls, ball *balls)
{
	int i;
	vector total = {0,0,0};
	for (i = 0; i < num_balls; i++)
	{
		vector_add(&total, &balls[i].v, &total);
	}
	return vector_abs(&total);
}


double statistics_mean_energy(int num_balls, ball *balls)
{
	int i;
	double mean = 0, energy;
	for (i = 0; i < num_balls; i++)
	{
		energy = vector_abs_squared(&balls[i].v);
		energy = energy / (num_balls * 2.0);
		mean += energy;
	}
	return mean;
}

double statistics_mean_momentum(int num_balls, ball *balls)
{
	int i;
	double mean = 0, momentum;
	for (i = 0; i < num_balls; i++)
	{
		momentum = vector_abs(&balls[i].v);
		momentum = momentum / (num_balls * 1.0);
		mean += momentum;
	}
	return mean;
}

void statistics_velocity_histogram(world *w, const char *path)
{
	char filename[256];
	double velocity;
	int i;

	sprintf(filename, "%s/velocities-%09ld.dat", path, w->step_count);
	
	FILE *output = fopen(filename, "w");
	if (output == NULL)
	{
		ERROR("Error opening file '%s'", filename);
		return;
	}
	fprintf(output, "###format\n#step_count\n#tot_num_coll\n#velocities\n");
	fprintf(output, "%ld \n", w->step_count);
	fprintf(output, "%ld \n", w->num_collisions);
	for (i=0; i < w->num_balls; i++)
	{
			velocity = vector_abs(&(w->balls[i].v));
			fprintf(output, "%f\n", velocity);
	}
	fclose(output);

}
