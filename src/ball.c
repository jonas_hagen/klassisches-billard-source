#include "ball.h"

int ball_collision(ball *ball1, ball *ball2, double ball_radius)
{
	vector v1, v2, r1, r2, v1rf, v2rf;
	vector n, distance;
	v1=ball1->v;
	r1=ball1->r;
	v2=ball2->v;
	r2=ball2->r;
	
	//Berechnen der Stossnormale n
	vector_subtract(&r2,&r1,&distance);
	
	//detect if they are close enough to collide
	if(vector_abs_squared(&distance) > 4 * ball_radius*ball_radius)
	{
		return 0;
	}

	if(vector_compare(&r1,&r2)==TRUE)
	{
		//ERROR("Two Balls at the same place not proceeding collision");
		return 0;
	}

	//Check if a collision is valid by checking if one angle is smaller than
	//90 degrees
	double alpha1, alpha2;
	alpha1 = vector_scalar_product(&distance, &v1);
	alpha2 = -vector_scalar_product(&distance,&v2);
	if (alpha1 < 0 && alpha2 < 0)
	{
		return 0;
	}
	
	//normalize stossnormale
	vector_normalize(&distance,&n);

	//restframe of ball2
	vector_subtract(&v1,&v2,&v1rf);
	vector_init(&v2rf);

	//collisionin in restframe of ball2:
	vector_multiply(vector_scalar_product(&v1rf,&n),&n,&v2rf);
	vector_subtract(&v1rf,&v2rf,&v1rf);
	
	//return to labframe and write;
	vector_add(&v1rf,&v2,&(ball1->v));
	vector_add(&v2rf,&v2,&(ball2->v));

	DEBUG("Collision!");
	
	return 1;
}

void ball_reflect(ball *b, int plane)
{
	if(plane==1)
	{
		b->v.x = - b->v.x;
	}
	else if(plane == 2)
	{
		b->v.y = - b->v.y;
	}
	else if(plane==3)
	{
		b->v.z = - b->v.z;
	}
	else
	{
		ERROR("Reflection on an invalid plane WARNING\n");
	}
}

void ball_move(ball *b, const double timestep)
{
	b->r.x+=b->v.x * timestep;
	b->r.y+=b->v.y * timestep;
	b->r.z+=b->v.z * timestep;
}

double ball_energy(ball *b)
{
	return vector_abs_squared(&(b->v))/2;
}


void ball_init(ball*b)
{
	b->r.x=0;
	b->r.y=0;
	b->r.z=0;
	b->v.x=0;
	b->v.y=0;
	b->v.z=0;
}

void ball_test()
{
	double timestep=2;
	double ball_radius=10;

	ball ball1, ball2;
	ball_init(&ball1);
	ball_init(&ball2);
	
	ball2.r.x=1;
	ball2.r.y=1;
	
	ball1.v.x=1;
	ball1.v.y=0;
	ball1.v.z=0;


	printf("before collision: \n");
	vector_printvector(&(ball1.r));
	vector_printvector(&(ball1.v));

	vector_printvector(&(ball2.r));
	vector_printvector(&(ball2.v));
	
	ball_collision(&ball1,&ball2,ball_radius);
	
	printf("aftercollision: \n");
	vector_printvector(&(ball1.r));
	vector_printvector(&(ball1.v));

	vector_printvector(&(ball2.r));
	vector_printvector(&(ball2.v));
	
	ball_reflect(&ball1,2);
	printf("\nreflection on plane 2\n");
	vector_printvector(&(ball1.v));

	printf("\n move ball1  with timestep = 1");
	ball_move(&ball1,timestep);
	vector_printvector(&(ball1.r));

	printf("\n kinetic energy of ball1 is %f", ball_energy(&ball1));
}
