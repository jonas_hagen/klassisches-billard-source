#include "world.h"

int world_save_to_file(world *w, const char *path)
{
	int i;
	char filename[256];
	
	sprintf(filename, "%s/world-%06ld.world",
		path, w->step_count);
	
	FILE *output = fopen(filename, "w");
	if (NULL == output)
	{
		ERROR("Error opening file '%s'", filename);
		return -1;
	}

	// Print the settings
	fprintf(output, "timestep %g\n", w->timestep);
	fprintf(output, "ball_radius %g\n", w->ball_radius);
	fprintf(output, "box_width %g\n", w->box_width);
	fprintf(output, "gravity %g\n", w->gravity);
	fprintf(output, "num_steps %ld\n", w->num_steps);
	fprintf(output, "step_count %ld\n", w->step_count);
	fprintf(output, "boundaries %f %f %f\n",
		w->boundaries.x,
		w->boundaries.y,
		w->boundaries.z);

	for (i = 0; i < w->num_balls; i++)
	{
		fprintf(output, "%f %f %f ",
			w->balls[i].r.x,
			w->balls[i].r.y,
			w->balls[i].r.z);
		fprintf(output, "%f %f %f\n",
			w->balls[i].v.x,
			w->balls[i].v.y,
			w->balls[i].v.z);
	}
	
	DEBUG("Wrote %i balls to '%s'.", i, filename);

	fclose(output);
	return w->num_balls;
}

int world_save_to_povray(world *w, const char *path)
{
	int i, red, blue;
	char filename[256];
	
	sprintf(filename, "%s/scene-%06ld.pov",
		path, w->step_count);

	FILE *output = fopen(filename, "w");
	if (NULL == output)
	{
		ERROR("Error opening file '%s'", filename);
		return -1;
	}

	// set the camera
	fprintf(output, "camera {\n\
		location <%f,%f,%f>\n\
		look_at <0,0,0>\n}\n",
		w->boundaries.x*1.3,
		w->boundaries.y*1.2,
		w->boundaries.z*1.3);
	// set the light source
	fprintf(output, "light_source {\n\
		<%f,%f,%f>\n\
		color rgb <1,1,1>\n}\n\n",
		w->boundaries.x*1.2,
		w->boundaries.y*1.8,
		w->boundaries.z*1.2);
	// box
	/*fprintf(output, "box {<%f,%f,%f>, <0,0,0> pigment {color rgb <1,0,0>}}\n",
		w->boundaries.x,
		w->boundaries.y,
		w->boundaries.z);	*/

	// set the balls
	for (i = 0; i < w->num_balls; i++)
	{
		if (i%10 == 0)
		{
			red = 0;
			blue = 1;
		}
		else
		{
			red = 1;
			blue = 0;
		}
		fprintf(output,
			"sphere {<%f,%f,%f>, %f pigment {color rgb <%d,0,%d>}}\n",
			w->balls[i].r.x,
			w->balls[i].r.y,
			w->balls[i].r.z,
			w->ball_radius,
			red,blue);
		/*fprintf(output, "%f %f %f\n",
			w->balls[i].v.x,
			w->balls[i].v.y,
			w->balls[i].v.z);
		*/
	}
	
	fclose(output);
	DEBUG("Wrote %i balls to '%s'.", i, filename);

	return w->num_balls;
}


int world_load_from_file(world *w, const char *filename)
{
	int lines = 0, i = 0;
	double x=0, y=0, z=0, q=0, r=0, s=0;
	w->box_width = 10; // TODO: initialize in other method

	world_free(w);
	if (world_load_config(w, filename) == 1)
		return -1;
	
	FILE *input = fopen(filename, "r+");
	if (NULL == input)
	{
		ERROR("Error opening file '%s'", filename);
		return -1;
	}

	// Count lines in order to allocate memory
	while (EOF != (fscanf(input, "%*[^\n]"), fscanf(input, "%*c"))) 
    	++lines;

	// Allocate memory for the boxes
	w->num_boxes.x = (unsigned int) (w->boundaries.x/w->box_width + 1);
	w->num_boxes.y = (unsigned int) (w->boundaries.y/w->box_width + 1);
	w->num_boxes.z = (unsigned int) (w->boundaries.z/w->box_width + 1);
	w->num_boxes_total =
		w->num_boxes.x * w->num_boxes.y * w->num_boxes.z;
	w->boxes = (ball**) calloc(w->num_boxes_total, sizeof(ball*));
	DEBUG("Boundaries are %g %g %g", x, y, z);
	DEBUG("%d boxes are needed.", w->num_boxes_total);
	
	// Allocate memory
	w->num_balls = lines - WORLD_NUM_CONFIG_LINES;
	DEBUG("We need memory for %d balls.", w->num_balls);
	w->balls = (ball*) calloc(w->num_balls, sizeof(ball));
	if (NULL == w->balls)
	{
		ERROR("Failed to allocate memory.");
		return -1;
	}

	// Drop the config lines
	rewind(input);
	for (i = 0; i < WORLD_NUM_CONFIG_LINES; i++)
		(fscanf(input, "%*[^\n]"), fscanf(input, "%*c"));

	// Fill the data in
	i = 0;
	while (EOF != 
		fscanf(input, "%lf %lf %lf %lf %lf %lf\n", 
		&x, &y, &z, &q, &r, &s))
	{
		w->balls[i].r.x = x;
		w->balls[i].r.y = y;
		w->balls[i].r.z = z;
		w->balls[i].v.x = q;
		w->balls[i].v.y = r;
		w->balls[i].v.z = s;
		w->balls[i].next = NULL;
#ifdef DEBUG_ON
		w->balls[i].number = i;
#endif
		i++;
	}

#ifdef DEBUG_ON
	for (i = 0; i < w->num_balls; i++)
	{
		DEBUG("%d:   r = (%g, %g, %g)   v = (%g, %g, %g)",
			i,
			w->balls[i].r.x,
			w->balls[i].r.y,
			w->balls[i].r.z,
			w->balls[i].v.x,
			w->balls[i].v.y,
			w->balls[i].v.z);
	}
#endif
	

	fclose(input);
	return w->num_balls;
}

int world_load_config(world *w, const char *filename)
{
	FILE *input = fopen(filename, "r+");
	char buffer[255];
	BOOL check = TRUE;
	int ret;

	if (NULL == input)
	{
		ERROR("Error opening file '%s'", filename);
		return -1;
	}

	// Total number of config files must correspond to the
	// value in WORLD_NUM_CONFIG_LINES !

	// timestep
	ret = fscanf(input, "%s %lf\n", buffer, &(w->timestep));
	check = check && (strcmp("timestep", buffer) == 0) && ret == 2;
	// ball_radius
	ret = fscanf(input, "%s %lf\n", buffer, &(w->ball_radius));
	check = check && (strcmp("ball_radius", buffer) == 0) && ret == 2;
	// gravity
	ret = fscanf(input, "%s %lf\n", buffer, &(w->gravity));
	check = check && (strcmp("gravity", buffer) == 0) && ret == 2;
	// box_width
	ret = fscanf(input, "%s %lf\n", buffer, &(w->box_width));
	check = check && (strcmp("box_width", buffer) == 0) && ret == 2;
	// num_steps
	ret = fscanf(input, "%s %ld\n", buffer, &(w->num_steps));
	check = check && (strcmp("num_steps", buffer) == 0) && ret == 2;
	// step_count
	ret = fscanf(input, "%s %ld\n", buffer, &(w->step_count));
	check = check && (strcmp("step_count", buffer) == 0) && ret == 2;
	// boundaries
	ret = fscanf(input, "%s %lf %lf %lf\n", buffer,
		&(w->boundaries.x),
		&(w->boundaries.y),
		&(w->boundaries.z));
	check = check && (strcmp("boundaries", buffer) == 0) && ret == 4;

	if (!check)
	{
		ERROR("Settings are not well formated!");
		fclose(input);
		return 1;
	}

	INFO("Settings: timestep=%g, ball_radius=%g, box_width=%g",
		w->timestep,
		w->ball_radius,
		w->box_width);
	INFO("          num_steps=%ld, step_count=%ld, gravity=%g", 
		w->num_steps,
		w->step_count,
		w->gravity);
	INFO("Boundaries: (%g, %g ,%g)",
		w->boundaries.x, w->boundaries.y, w->boundaries.z);

	fclose(input);
	return 0;
}

