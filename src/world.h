#ifndef WORLD
#define WORLD

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <math.h>

#include "global.h"
#include "vector.h"
#include "ball.h"

#define WORLD_NUM_CONFIG_LINES 7

struct world_s
{

	double box_width;
	double timestep;
	double ball_radius;

	double gravity;

	unsigned long step_count;
	unsigned long num_steps;
	unsigned long num_collisions;

	// An array of all balls
	ball *balls;
	// The number of balls
	int num_balls;

	// An array of linked lists of all balls for each box
	ball **boxes;
	// Number of boxes used
	unsigned int num_boxes_total;
	ivector num_boxes;

	// The Boundaries of the world
	vector boundaries;

	// statistics
	double mean_force;
	double mean_force_top;
	double mean_force_bottom;

};

typedef struct world_s world;

/* 
 * Reads balls from a .world file.
 *
 * The file is expectet to have the following format:
 * 0						// step_count
 * bx by bz					// boundaries
 * r1x r1y r1z v1x v1y v1z	// location and velocity of ball 1
 * r2x r2y r2z v2x v2y v2z	// same for ball 2
 * ...
 */
int world_load_from_file(world *w, const char *filename);

/*
 * Reads the configuration from a file
 *
 * The file must have the following format (ordering matters):
 * timestep 1.0
 * ball_radius 1.0
 * box_width 10.0
 * num_steps 1000
 */
int world_load_config(world *w, const char *filename);

int world_save_to_file(world *w, const char *path);
int world_save_to_povray(world *w, const char *path);
void world_free(world *w);
unsigned int world_box_index(world *w, ivector position);
void world_move(world *w);
void world_assign_boxes(world *w);
void world_resolve_collisions(world *w);
void world_init(world *w, double timestep, double box_width, double ball_radius);
void world_wall_collisions(world *w);

/* 
 *returns True if the coordinate in position points to a valid box. 
 */
BOOL world_box_is_valid(world *w, ivector *position);

void world_test();

#endif
