#ifndef STATISTICS_H
#define STATISTICS_H

#include "vector.h"
#include "global.h"
#include "ball.h"
#include "world.h"

double statistics_total_energy(int num_balls, ball *balls);
double statistics_total_momentum(int num_balls, ball *balls);
double statistics_mean_energy(int num_balls, ball *balls);
double statistics_mean_momentum(int num_balls, ball *balls);
double statistics_log_speed(int num_balls, ball *balls);
void statistics_velocity_histogram(world *w, const char *path);
#endif
