#ifndef GLOBAL_H
#define GLOBAL_H

enum BOOL_e {FALSE = 0, TRUE = 1};

typedef enum BOOL_e BOOL;

//#define boltzman_const 1.380658e-23
#define boltzman_const 1

//Debug Makro
//#define DEBUG_ON
#ifdef DEBUG_ON
	#include <stdio.h>
	#define DEBUG(...) {printf("DEBUG   ");printf(__VA_ARGS__);printf("\n");fflush(stdout);}
#else
	#define DEBUG(...) {}
#endif

#define INFO_ON
#ifdef INFO_ON
	#include <stdio.h>
	#define INFO(...) {printf("INFO    ");printf(__VA_ARGS__);printf("\n");fflush(stdout);}
#else
	#define INFO(...) {}
#endif

#define ERROR_ON
#ifdef ERROR_ON
	#include <stdio.h>
	#define ERROR(...) {printf("\033[1m\033[7;31mERROR   ");printf(__VA_ARGS__);printf("\033[0m\n");fflush(stdout);}
#else
	#define ERROR(...) {}
#endif

#endif /* GLOBAL_H */
