#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "global.h"
#include "vector.h"
#include "ball.h"
#include "world.h"
#include "statistics.h"

void billard_play(const char *argv[])
{
	INFO("***************************************");
	INFO("\tproceeding %s",argv[1]);
	INFO("***************************************")

	world w;
	unsigned long i, num_steps;
	double energy1, momentum1;
	double energy2, momentum2;
	double energy3, x, y, z;
	double meanenergy, temperature;
	clock_t start, stop;

	world_init(&w, 0, 0, 0);
	if (world_load_from_file(&w, argv[1]) < 0)
		return;
	num_steps = w.num_steps;
	DEBUG("Num boxes: %d %d %d", w.num_boxes.x,  w.num_boxes.y,  w.num_boxes.z);
	
	energy1 = statistics_total_energy(w.num_balls, w.balls);
	momentum1 = statistics_total_momentum(w.num_balls, w.balls);
	INFO("Initial: E = %g, p = %g", energy1, momentum1);
	INFO("Running %ld steps ... ", num_steps);
	start = clock();
	for (i = 0; i < num_steps; i++)
	{
		if (i % 50 == 0)
		{
			//stop = clock();
			//INFO("	Step %ld complete. Time elapsed: %g seconds",
			//	i, (double)(stop-start)/CLOCKS_PER_SEC);
			world_save_to_povray(&w, argv[2]);
			statistics_velocity_histogram(&w, argv[2]);
		}
		world_wall_collisions(&w);
		world_assign_boxes(&w);
		world_resolve_collisions(&w);
		world_move(&w);
	}
	stop = clock();
	INFO("Done. Total time elapsed: %g seconds.",
		(double)(stop-start)/CLOCKS_PER_SEC);
	
	world_save_to_file(&w, argv[2]);
	statistics_velocity_histogram(&w, argv[2]);

	energy2 = statistics_total_energy(w.num_balls, w.balls);
	momentum2 = statistics_total_momentum(w.num_balls, w.balls);
	meanenergy = energy2/w.num_balls;
	temperature = meanenergy*2/3* 1/ boltzman_const;
	INFO("Final: E = %g, p = %g", energy2, momentum2);
	INFO("Difference: dE = %g, dp = %g", energy2-energy1, momentum2-momentum1);
	x = w.boundaries.x;// - 2.0 * w.ball_radius;
	y = w.boundaries.y;// - 2.0 * w.ball_radius;
	z = w.boundaries.z;// - 2.0 * w.ball_radius;
	energy3 = w.mean_force*x*y*z/(2.0*(x*y+x*z+y*z))*3.0/2.0;
	INFO("Mean force = %g => E = %g, dE = %g",
		w.mean_force, energy3, energy3-energy2);
	INFO("Mean force: %g", w.mean_force);
	INFO("Mean energy: %g", meanenergy);
	INFO("Temparature: T = %g kb", temperature);
	INFO("Pressure top: %g", w.mean_force_top);
	INFO("Pressure bottom: %g", w.mean_force_bottom);
	INFO("Total number of collisions N = %ld", w.num_collisions);
	INFO("****************************************************");
}

void usage(void)
{
	printf("\nUsage:\n");
	printf("	billard [WORLD] [PATH]\n");
	printf("With:\n");
	printf("	WORLD: The initial world file.\n");
	printf("	PATH: The path where all output should be stored.\n");
	printf("\nHave fun!\n");
}
 
int main(int argc, const char* argv[])
{
	if (argc < 3)
	{
		ERROR("Some parameters are missing.");
		usage();
		return EXIT_FAILURE;
	}
   	billard_play(argv);

    return EXIT_SUCCESS;
}
