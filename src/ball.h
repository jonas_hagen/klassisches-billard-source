#ifndef BALL
#define BALL

#include "vector.h"
#include "global.h"

typedef struct ball_s ball;
struct ball_s
{
	vector r; // location
	vector v; // velocity
	ball *next; // linked list
#ifdef DEBUG_ON
		int number; 
#endif
};

/* 
 * Computes change in velocity for an elastic collision.
 * returns 1 if a collision took place and 0 if not.
 */
int ball_collision(ball *ball1, ball *ball2, double ball_radius);

/* 
 * Reflects ball b on plane 1, 2 or 3.
 */
void ball_reflect(ball *b, int plane);
void ball_move(ball *b, const double timestep);
double ball_energy(ball *b);
void ball_init(ball *b);

void ball_test();

#endif
